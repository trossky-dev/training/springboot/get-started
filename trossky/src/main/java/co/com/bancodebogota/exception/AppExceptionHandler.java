package co.com.bancodebogota.exception;

import co.com.bancodebogota.ui.models.response.ErrorMessages;
import org.springframework.beans.TypeMismatchException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Date;

@ControllerAdvice
public class AppExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(value = {Exception.class})
	protected ResponseEntity<Object> handleAnyException(Exception ex,WebRequest request) {
		return getObjectResponseEntity(ex.getLocalizedMessage(), ex.toString());
	}
	@ExceptionHandler(value = {NullPointerException.class, UserServiceException.class})
	protected ResponseEntity<Object> handleSpecificExceptions(Exception ex,WebRequest request) {
		return getObjectResponseEntity(ex.getLocalizedMessage(), ex.toString());
	}


	private ResponseEntity<Object> getObjectResponseEntity(String localizedMessage, String s) {
		String errorMessageDescription = localizedMessage;
		if (errorMessageDescription == null) errorMessageDescription = s;
		ErrorMessages errorMessages = new ErrorMessages(new Date(), errorMessageDescription);

		return new ResponseEntity<>(
				errorMessages, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
	}
}
