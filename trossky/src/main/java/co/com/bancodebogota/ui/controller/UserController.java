/**
 * 
 */
package co.com.bancodebogota.ui.controller;

import co.com.bancodebogota.exception.UserServiceException;
import co.com.bancodebogota.ui.models.response.UserModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

/**
 * @author tky
 *
 */
@RestController
@RequestMapping(path = "/users", consumes = {MediaType.APPLICATION_JSON_VALUE},produces = {MediaType.APPLICATION_JSON_VALUE})
public class UserController {
	public static  HashMap<String, UserModel> users;


	@GetMapping
	public ResponseEntity<List<UserModel>> getUsers() {
		if(users!=null){
			List<UserModel> userModels = new ArrayList<>(users.values());
			return new ResponseEntity<>(userModels,HttpStatus.OK);
		}else{
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

	@GetMapping(path="/{id}")
	public ResponseEntity<UserModel> getUser(@PathVariable String id) {
	if(true) throw new UserServiceException("A service exception is thrown");

		if(users.containsKey(id)) {
			return new ResponseEntity<>(users.get(id),HttpStatus.OK);
		}else {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}


	}


	@PostMapping
	public ResponseEntity<UserModel> createUser(@Valid @RequestBody UserModel userModel ) {
		// HttpStatus.UNPROCESSABLE_ENTITY
		UserModel model= new UserModel();
		model.setEmail(userModel.getEmail());
		model.setFirstName(userModel.getFirstName());
		model.setLastName(userModel.getLastName());
		model.setPassword(userModel.getPassword());

		String _uuid= UUID.randomUUID().toString();
		model.setId(_uuid);
		System.out.println(_uuid);

		if(users==null)users= new HashMap<String, UserModel>();
		users.put(_uuid, model);

		return new ResponseEntity<>(model,HttpStatus.OK);
	}

	@PutMapping(path="/{id}")
	public ResponseEntity<UserModel> updateUser(@PathVariable String id, @Valid @RequestBody UserModel userModel) {
		UserModel model= users.get(id);
		model.setEmail(userModel.getEmail());
		model.setFirstName(userModel.getFirstName());
		model.setLastName(userModel.getLastName());
		model.setPassword(userModel.getPassword());

		return new ResponseEntity<UserModel>(HttpStatus.OK);
	}

	@DeleteMapping(path="/{id}")
	public ResponseEntity<Void> deleteUser(@PathVariable String id) {


		if(users.containsKey(id)) {
			users.remove(id);

			return ResponseEntity.noContent().build();
		}else {
			return ResponseEntity.notFound().build();
		}



	}

}
