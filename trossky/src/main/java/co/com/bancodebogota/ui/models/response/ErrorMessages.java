package co.com.bancodebogota.ui.models.response;

import java.util.Date;

public class ErrorMessages {
	private Date timestap;
	private String message;

	public ErrorMessages() {
	}

	public ErrorMessages(Date timestap, String message) {
		this.timestap = timestap;
		this.message = message;
	}

	public Date getTimestap() {
		return timestap;
	}

	public void setTimestap(Date timestap) {
		this.timestap = timestap;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
