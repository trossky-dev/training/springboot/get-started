package com.trossky.api.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.trossky.api.model.Greeting;

// 
@RestController
public class GreetingController {
	 
	@RequestMapping("/greeting")
	public Greeting greeting() {
		return new Greeting(1, "Hello, User");
	}
}
