package com.example.demo.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.service.PersonService;

@RestController
public class DemoController {
	
	@Autowired
	private PersonService personService;
	
	@RequestMapping(value="/person", method=RequestMethod.GET)
	public PersonService getPersons() {
		
		personService.setId("1");
		personService.setName("Luis");
		personService.setAge("25");
		personService.setHeight("1.63 cm");
		
		return personService;
	}

}
