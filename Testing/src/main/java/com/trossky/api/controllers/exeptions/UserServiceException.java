package com.trossky.api.controllers.exeptions;

public class UserServiceException extends RuntimeException {
	
	private static final long serialVersionUID= 1248771109171435607L;
	
	public UserServiceException(String message) {
		super(message);
	}

}
